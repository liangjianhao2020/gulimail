package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author liang
 * @email liang@gmail.com
 * @date 2022-06-10 11:58:56
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
