package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author liang
 * @email liang@gmail.com
 * @date 2022-06-10 12:46:09
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
